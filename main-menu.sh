#inicializando variaveis
DIALOG_CANCEL=1
DIALOG_ESC=255
HEIGHT=0
WIDTH=0
main_title="INSTALAR E CONFIGURAR - AMBIENTE"
# Caminho completo até as listas de pacotes...
pkgs_path="$(pwd)/lists/chk_list/"

# Título comum a todas as telas...
#main_title="$line
#INSTALAR E CONFIGURAR - AMBIENTE: ${ambiente^^} - RELEASE: ${release^^}
#$line"
# Função para continuar...
continuar() {
    dialog --yesno "Podemos continuar" 0 0
    
    #read -p "Podemos continuar (s/N)? " resp
    #[[ ${resp,,} != "s" ]]
}




# Refaz as listas de pacotes...
read_pkgs() {
    pkgs_list=$(grep -vE "^\s*#" $1 | sed '/^\s*$/d')
    pkgs_apti=$(tr "\n" " " <<< $pkgs_list)
}

# Tela de instalação de pacotes...
instalar_pacotes() {
    # Monta lista de pacotes para exibição e instalação...
    # Tela de instalação...
    clear
    
    inst=$(
    dialog --stdout --checklist "Pacotes" 0 0 0 --file "${pkgs_path}$1"
    )
    
    dialog --backtitle "$main_title" --yesno "Os seguintes pacotes serão instalados: \n\n$inst \n\n	Continuar a Instalação?" 0 0 && op=0 || op=1
    if [ $op -eq 0 ]; then
        echo "$instalando"
        sudo apt install -y $testing $inst
        if (($?)); then
            echo -e "\n$line\nA instalação falhou!\nVerifique a lista de pacotes '$1' e tente novamente.\n$line\n"
        else
            echo -e "\n$line\nSucesso!\n$line\n"
        fi
        dialog --msgbox "Tecle 'enter' para continuar... " 0 0

    fi
}

# Instala o Firefox fornecido diretamente pela Mozilla...
instalar_firefox() {
    clear
    ff_launcher="$(pwd)/pool/firefox/firefox-quantum.desktop"
    echo -e "Esta opção instala a última versão do Firefox diretamente da Mozilla.\n"
    dialog  --backtitle "$main_title" --title "Instalar Firefox" --yesno "Esta opção instala a última versão do Firefox diretamente da Mozilla. Gostaria de Constinuar?" 10 50 && in=0 || in=1
    if [ "$in" == 0 ]; then
	echo "$instalando"
	wget -O firefox.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=pt-BR"
	tar -xvf firefox.tar.bz2
	sudo mv firefox /opt/
	rm firefox.tar.bz2
	sudo ln -s /opt/firefox/firefox /usr/bin/firefox
	sudo cp $ff_launcher /usr/share/applications/
	read -p "Tecle 'enter' para continuar... " segue
	break
    fi
}

# Instalar o Steam...
instalar_steam() {
    clear
    echo "$main_title"
    echo -e "Esta opção ativa a arquiterura i386 e instala o cliente Steam.\n"
        dialog  --backtitle "$main_title" --title "Instalar a Steam" --yesno "Esta opção ativa a arquiterura i386 e instala o cliente Steam. Gostaria de Constinuar?" 10 50 && in=0 || in=1
    if [ $in -eq 0 ]; then
	echo -e "\nInstalando Steam...\n"
	sudo dpkg --add-architecture i386
	sudo apt update
	sudo apt install steam
	dialog --msgbox "Tecle 'enter' para continuar... " 10 50
    fi
}

# Configuração do desktop...
desktop_settings() {
    clear
    dialog --backtitle "$main_title" --title "Configurando o Desktop" --yesno "Gostaria de usar as Configurações pre definidas do instalador?" 10 50 && in=0 || in=1
    if [ $in -eq 0 ]; then
    echo -e "Esta opção aplicará as configurações do ambiente $ambiente"

    desktops/$ambiente/desktop-settings.sh "$(pwd)" $ambiente
    dialog --backtitle "$main_title" --title "Configurando o Desktop" --msgbox "Ambiente Configurado...Aproveite!"
    
    else 
    	dialog --backtitle "$main_title" --title "Configurando o Desktop" --yesno "Tem certeza?" 5 50  && dialog --msgbox "Aproveite Bem o Sistema" 5 50 --and-widget --sleep 3 --infobox "saindo" 0 0  || desktop_settings 
    fi
    
}



while true; do 
  op=$(dialog  --stdout --no-shadow --title "Menu" --backtitle "$main_title" --cancel-label "CANCEL" --menu "Select:" $HEIGHT $WIDTH 10 \
   "1" "Firmware"\
   "2" "Base e utilitários do" ${ambiente^^}\
   "3" "Utilitários de sistema"\
   "4" "Internet"\
   "5" "Firefox da Mozilla"\
   "6" "Multimídia"\
   "7" "Escritório"\
   "8" "Gráficos"\
   "9" "Desenvolvimento"\
  "10" "Virtualização"\
  "11" "Jogos"\
  "12" "Steam"\
   "13" "Configuração do ambiente gráfico"\
   "14" "Reiniciar a máquina"\
   "15" "Sair")

	case $op in
	    1) instalar_pacotes 01-firmware-non-free;;
	    2) instalar_pacotes 02-base-desktop-$ambiente-$release;;
	    3) instalar_pacotes 03-system-utilities;;
	    4) instalar_pacotes 04-network-$release;;
	    5) instalar_firefox ;;
	    6) instalar_pacotes 06-multimedia-$release;;
	    7) instalar_pacotes 07-escritorio;;
	    8) instalar_pacotes 08-graficos-$release;;
	    9) instalar_pacotes 09-desenvolvimento;;
           10) instalar_pacotes 10-virtualizacao;;
	   11) instalar_pacotes 11-jogos;;
	   12) instalar_steam;;
	   13) desktop_settings $ambiente;;
	   14) sudo reboot;;
	   15) clear && exit;;
	   "") clear && exit;;

	esac
done


#



